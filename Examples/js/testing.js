/* RENDER JSON SETTINGS */
renderjson.set_show_to_level(2);

/* INIT SERIALIZERS */
var jsonSerializer = new eoq2.serialization.JsonSerializer();
var textSerializer = new eoq2.serialization.TextSerializer();
// serializer3 = PySerializer() not implemented
// serializer4 = JsSerializer() not implemented

/* MISC */
function exists(id) {
    let element = document.getElementById(id);
    if (element===null){
        return false;
    }else{
        return true;
    }
}

function PushIfUnique(arr,element){
    if (arr.includes(element)){
        return arr;
    }else{
        arr.push(element);
        return arr;
    }
}

/* PRINTING FUNCTIONS */
function Print(msg, id="") {
    $("#console").append('<div id='+id+'>'+ msg + '<br></div>');
}

function PrintResult(result, testNr){
    let element = "<span><span style='font-weight: bold;' class='passed'>TEST PASSED, RETURNING:</span> "+textSerializer.Ser(result)+ "</span><br>";
    $("#" + testNr.toString()).append(element);
}

function PrintFailedCmd(error, testNr){
    let element = "<span><span style='font-weight: bold;' class='failed'>TEST FAILED, THROWING:</span> "+ error + "</span><br>";
    $("#" + testNr.toString()).append(element);
}

/* ELEMENTS */
function CreateStatusBlock(){
    if (!exists("status_block")) {
        let element = "<div class='message' id='status_block'>" +
            "<h4><span id='status-light' class='passed'>&#9679;</span> TESTING STATUS</h4>" +
            "<p id ='status'> No test data yet! </p>" +
            "</div>";
        $("#console").append(element);
    }
}

function UpdateStatusBlockOnlyText(msg, append) {
    if (append) {
        document.getElementById("status").innerHTML = document.getElementById("status").innerHTML + "<br>" + msg;
    }else {
        document.getElementById("status").innerHTML = msg;
    }
}

function SetStatusPassed(){
    document.getElementById("status-light").setAttribute("class","passed");
}

function SetStatusFailed(){
    document.getElementById("status-light").setAttribute("class","failed");
}

function UpdateStatusBlock(testNr, cmds, failedTests){
    let msg ="Test run on " + (parseInt(testNr)+1).toString() + " of " + cmds.length + " cmds.<br>";
    if (failedTests.length){
        SetStatusFailed()
        msg += "Testing <span class='failed'>failed</span> on " + failedTests.length + " of " + cmds.length + " cmds.<br>";
        msg += "Jump to failed tests at ";
        for (let failed in failedTests){
            msg += "<a href='#"+failedTests[failed]+"'>"+failedTests[failed]+"</a>,";
        }
    }else{
        SetStatusPassed()
        if (testNr == cmds.length-1){
            msg += "Testing <span class='passed'>passed</span> on all cmds!";
        }
    }
    document.getElementById("status").innerHTML = msg;
}

function PrintCmdInfo(cmd, testNr){
    if (!exists(testNr)) {
        // put test div
        let element = '<div class="message" id=' + testNr + '><h4>TEST #' + testNr + '</h4></div>'
        $("#console").append(element);//"<div id='"+testNr.toString()+"'><span style='font-weight: bold;'>### TEST "+testNr+" ###</span>"+'>'+0+'</div>')
        // append txt
        let cmd2textual = textSerializer.Ser(cmd);
        $("#" + testNr.toString()).append("<pre>CMD (TXT PASS 1): " + cmd2textual + "</pre>");
        let cmd2 = textSerializer.Des(cmd2textual);
        let cmd2textual2 = textSerializer.Ser(cmd2);
        let txtDeviates = cmd2textual==cmd2textual2;
        $("#" + testNr.toString()).append("<pre>CMD (TXT PASS 2): " + cmd2textual2 + (txtDeviates?"":" -> DEVIATES") + "</pre>");
        // append json
        $("#" + testNr.toString()).append("<p>CMD (CMD):</p>");
        document.getElementById(testNr).appendChild(renderjson(cmd2));
        $("#" + testNr.toString()).append("<br>");
    }
}

/* JSEOQ CMDs */

async function TestCommand(domain,cmd) {
    // test the serialization and deserialization
    // let cmd2textual = textSerializer.Ser(cmd);
    // let textual2cmd = textSerializer.Des(cmd2textual);
    // submit the command to the domain
    return domain.Do(cmd);
}

function GetRoot(domain,modelfile) {
    return new Promise(function(resolve,reject) {
        let cmd = CMD.Get(QRY.Pth('resources').Sel(QRY.Pth('name').Equ(modelfile)).Idx(0).Pth('contents').Idx(0));
        let jsonCmd = jsonSerializer.Ser(cmd);
        // Print(jsonCmd);
        domain.Do(cmd).then(function(val) {
            resolve(val);
        }).catch(function(e) {
            Print("Getting the root for "+modelfile+" failed: "+ e.toString()+ "<br>");
            reject(e);
        });
    });
}