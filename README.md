# JsEOQ - The Javascript Implementation of EOQ

JsEoq is a Javascript implementation of EOQ. It is dedicated to client-side web applications. It fully implements EOQ queries, commands, results, and events. It includes domain wrappers that provide access to remote domains via HTTP or web sockets. Local domains are so far not supported.

## Contents
* eoq1: The library implementing EOQ version 1. This is every thing needed to use EOQ 1. (Depricated!)
* eoq2: The library implementing EOQ version 2. This is every thing needed to use EOQ 2.
* Examples: Example scripts that demonstrate using JsEOQ.

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 


